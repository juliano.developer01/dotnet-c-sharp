package br.com.testapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//Utilizado anotações Lombok para classe de entidade com código menos verboso
@Entity
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Livros implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String titulo;
	@Lob
	@Column(length=2147483647) 
	private String descricao;
	private String foto;

}

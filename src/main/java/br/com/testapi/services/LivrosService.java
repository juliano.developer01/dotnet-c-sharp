package br.com.testapi.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.testapi.domain.Livros;
import br.com.testapi.model.Imagens;
import br.com.testapi.model.Livraria;
import br.com.testapi.model.Livro;
import br.com.testapi.repositories.LivrosRepository;

@Service
public class LivrosService {
	
	@Autowired
	private LivrosRepository repo;

	public List<Livros> findAll() {
		return repo.findAll();
	}
	
	public Livros find(String id) {
		Optional<Livros> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + 
		Livros.class.getName(), null));
	}
	
	public void delete(String id) {
		find(id);
		repo.deleteById(id);
	}
	
	public Livros insert(String input) throws IOException {
		
		Livros obj = new Livros();
		Gson gson = new Gson();
				
		Livraria result = gson.fromJson(input, Livraria.class);
		//Aqui faço a verificação se consulta retornou algum resultado
		if(result.totalItems != 0) {
			for (Livraria.Volumes volumes : result.items) {
				
					obj.setId(volumes.id);
					Livro vol = gson.fromJson(volumes.volumeInfo, Livro.class);
					obj.setTitulo(vol.title);
					obj.setDescricao(vol.description);
					if(vol.imageLinks != null) {
						Imagens img = gson.fromJson(vol.imageLinks, Imagens.class);
						obj.setFoto(img.thumbnail);
					}
					obj = repo.save(obj);
			}
		
		}else {
			System.out.println("Não encontrado um Livro com esse Id para inclusão em favoritos!");
		}
			
		
		return obj;
	}
	
	public Page<Livros> resultPage(List<Livros> books, Integer page, Integer linesPerPage, String orderBy, String direction) {
		
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		int start = (int) pageRequest.getOffset();
		
		int end = (int) ((start + pageRequest.getPageSize()) > books.size() ? books.size()
				  : (start + pageRequest.getPageSize()));
		
		return new PageImpl<Livros>(books.subList(start, end), pageRequest, books.size());
	}
	
	public List<Livros> resultList(String input) throws IOException{
		Livros obj = new Livros();
		List<Livros> books = new ArrayList<Livros>();
		Gson gson = new Gson();
		
		Livraria result = gson.fromJson(input, Livraria.class);
		
		for (Livraria.Volumes volumes : result.items) {
			obj.setId(volumes.id);
			Livro vol = gson.fromJson(volumes.volumeInfo, Livro.class);
			obj.setTitulo(vol.title);
			obj.setDescricao(vol.description);
			if(vol.imageLinks != null) {
				Imagens img = gson.fromJson(vol.imageLinks, Imagens.class);
				obj.setFoto(img.thumbnail);
			}	
			books.add(obj);
			obj = new Livros();
			}
		
		return books;
	}
	
	public String json(InputStream input) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(input , "UTF-8"));
		
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

}

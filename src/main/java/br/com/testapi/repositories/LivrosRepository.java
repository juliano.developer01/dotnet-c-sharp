package br.com.testapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.testapi.domain.Livros;

@Repository
public interface LivrosRepository extends JpaRepository<Livros, String>{

}

package br.com.testapi.resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.testapi.domain.Livros;
import br.com.testapi.services.LivrosService;

@RestController
@RequestMapping(value="/api")
public class LivrosResource {
	
	@Autowired
	private LivrosService service;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Livros> find(@PathVariable String id) {
		Livros obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	

	
	@RequestMapping(value="/book/{id}/favorite", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value="/book/favorites", method=RequestMethod.GET)
	public ResponseEntity<List<Livros>> findAll() {
		List<Livros> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}
	
	
	@RequestMapping(value="/books", method=RequestMethod.GET)
	public ResponseEntity<Page<Livros>> searchByName( @RequestParam(required = false) String p ,
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="3") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="id") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) throws IOException{
		
		//Fazendo a requisição Http
		URL url = new URL("https://www.googleapis.com/books/v1/volumes?q="+p.replace( " ", "+" ));
			
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		if (con.getResponseCode() != 200) {
			System.out.println("Houve um erro na sua consulta");
		}
	
		InputStream rs = con.getInputStream();
		
		List<Livros> books = service.resultList(service.json(rs));
		Page<Livros> paginado = service.resultPage(books,page, linesPerPage, orderBy, direction);
		con.disconnect();
		return ResponseEntity.ok().body(paginado);
	}
	
	@RequestMapping(value="/book/{id}/favorite",method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@PathVariable String id) throws IOException {
	
		//Fazendo a requisição Http
		URL url = new URL("https://www.googleapis.com/books/v1/volumes?q="+id);
		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		
		if (con.getResponseCode() != 200) {
			System.out.println("Houve umn erro na sua consulta");
		}

		InputStream rs = con.getInputStream();
		
		service.insert(service.json(rs));
		con.disconnect();
		return ResponseEntity.noContent().build();
	}


}

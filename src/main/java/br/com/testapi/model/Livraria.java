package br.com.testapi.model;

import java.util.List;

import com.google.gson.JsonObject;


public class Livraria {
	
	public String kind;
    public Integer totalItems;
    public List<Volumes> items;
    
    public class Volumes {
    	public String id;
    	public JsonObject volumeInfo;
    	
    }
   
}

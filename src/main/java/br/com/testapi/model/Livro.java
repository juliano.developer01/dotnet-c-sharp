package br.com.testapi.model;

import com.google.gson.JsonObject;

public class Livro {

	public String title;
    public String description;
    public JsonObject imageLinks;
 
}
